// 3
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json));

// 4
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => json.map(({title}) => title))
.then((titles) => console.log(titles));


// 5
fetch("https://jsonplaceholder.typicode.com/todos/1",
{
	method: "GET",
	headers : {
		"Content-Type" : "application/json"
	},

})
.then((response) => response.json())
.then((json) => console.log(json));

// 7
fetch("https://jsonplaceholder.typicode.com/todos",
{
	method: "POST",
	headers : {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Created to Do List Item",
		userId: 1,
		Completed: false
	})

})
.then((response) => response.json())
.then((json) => console.log(json));

// 8
fetch("https://jsonplaceholder.typicode.com/todos/1",
{
	method: "PUT",
	headers : {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Updated to to list post",
		Description: "To update the my to do list with a different data structure",
		Status: "Pending",
		dateCompleted: "Pending",
		userId : 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",
{
	method: "PATCH",
	headers : {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "delectus aut autem",
		Status: "Complete",
		dateCompleted: "07/09/21",
		id : 1,
		completed : false,
		userId : 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",
{
	method: "DELETE"
});